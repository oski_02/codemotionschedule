var request = require("request"),
	jsonFile = require("jsonfile"),
	options = {
		url: 'https://www.koliseo.com/codemotion/codemotion-madrid/r4p/5632002325741568/agenda',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}, 
	response,
	schedule = {},
	simpleSchedule = {},
	days = [],
	fullScheduleFile = 'tmp/schedule.json',
	simpleScheduleFile = 'tmp/simpleSchedule.json';

console.log("Starting to steal codemotion data");

function setSpeeches(day, slotIndex) {
	var result = [];
	
	day.tracks.forEach(function(track) {
		track.slots.forEach(function(slot, i) {
			if (i === slotIndex) {
				var speech = {
					'track': track.name,
					'title': slot.contents.title,
					'description': slot.contents.description,
					'authors': slot.contents.authors
				};
				result.push(speech);
			}
		});
	});
	
	//console.log('speeches for day ', day.name, 'and slot', slotIndex, 'are :', result);
	
	return result;
}

function setSimpleSpeeches(day, slotIndex) {
	var result = [];
	
	day.tracks.forEach(function(track) {
		track.slots.forEach(function(slot, i) {
			if (i === slotIndex) {
				var speech = {
					'track': track.name,
					'title': slot.contents.title
				};
				result.push(speech);
			}
		});
	});
	
	//console.log('speeches for day ', day.name, 'and slot', slotIndex, 'are :', result);
	
	return result;
}

function setSlots(day) {
	var exampleTrack = day.tracks[0],
		result = [];
	
	exampleTrack.slots.forEach(function(s, i) {
		var slot = {
			'start': s.start,
			'end': s.end,
			'speeches': setSpeeches(day, i)
		}
		result.push(slot);
	});

	//console.log('slots for day ', day.name, 'are :', result);
	return result;
}

function setSimpleSlots(day) {
	var exampleTrack = day.tracks[0],
		result = [];
	
	exampleTrack.slots.forEach(function(s, i) {
		var slot = {
			'start': s.start,
			'end': s.end,
			'speeches': setSimpleSpeeches(day, i)
		}
		result.push(slot);
	});

	//console.log('slots for day ', day.name, 'are :', result);
	return result;
}

function setDays(days) {
	var result = [];
	days.forEach(function(d) {
		var day = {
			'name': d.name,
			'slots': setSlots(d)
		};
		result.push(day);
	});
	return result;
}

function setSimpleDays(days) {
	var result = [];
	days.forEach(function(d) {
		var day = {
			'name': d.name,
			'slots': setSimpleSlots(d)
		};
		result.push(day);
	});
	return result;
}

function parseSchedule(response){
	var days = response.days;
	
	schedule.days = setDays(days);
	simpleSchedule = setSimpleDays(days);
	//console.log('resultSchedule :', schedule);
}

function displayTrack(track) {
	console.log('track :', track.name);
	track.slots.forEach(function(slot) {
		console.log('slot: ', slot);
	});
}

function displayData(data) {
	var days = data.days;
	console.log("days: ", days);
	days.forEach(function(day) {
		var tracks = day.tracks;
		console.log('day: ', day.name);
		//console.log('tracks', tracks);
	});
	
}

function saveFullSchedule() {
	jsonFile.writeFile(fullScheduleFile, schedule, function(err) {
		console.log(err);
	});
}

function saveSimpleSchedule() {
	jsonFile.writeFile(simpleScheduleFile, simpleSchedule, function(err) {
		console.log(err);
	});
}
function callback(error, response, body) {
	response = (JSON.parse(body));
	
	//displayData(response);
	
	//displayTrack(response.days[0].tracks[0]);
	
	parseSchedule(response);
	
	saveFullSchedule();
	saveSimpleSchedule();
	
}

request(options, callback);

